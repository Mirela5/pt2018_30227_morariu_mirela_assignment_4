package TableModels;

import javax.swing.table.AbstractTableModel;

public class AccountModel extends AbstractTableModel
{
	private static final long serialVersionUID = 584852758754803197L;
	private Object[] columnNames;
	private Object[][] data;
	private String objectName;
	
	public AccountModel( Object[][] data)
	{
		this.columnNames = new Object[7];
		columnNames[0] = "ID";
		columnNames[1] = "Person ID";
		columnNames[2] = "Person";
		columnNames[3] = "Type";
		columnNames[4] = "Ammount";
		columnNames[5] = "Interest";
		columnNames[6] = "Currency";
		this.data = data;	
	}
	
	public void setData(Object[][] data) 
	{
		this.data = data;
		this.fireTableDataChanged();
	}
	
	@Override
	public int getColumnCount()
	{
        return columnNames.length;
    }
	
	@Override
    public boolean isCellEditable(int row, int column) 
	{ // custom isCellEditable function
        return column == 4 || column == 6;
    }
	
    public int getRowCount()
    {
        return data.length;
    }
    
    public String getColumnName(int col)
    {
        return columnNames[col].toString();
    }

    public Object getValueAt(int row, int col)
    {
        return data[row][col];
    }
    
    public void setValueAt(Object value, int row, int col)
    {
        data[row][col] = value;
        fireTableCellUpdated(row, col);
    }
    
    public String getObjectName() 
    {
    	return objectName;
    }
    
    

}
