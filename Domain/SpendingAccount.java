package Domain;


public class SpendingAccount extends Account 
{
	private static final long serialVersionUID = 7810906401320183362L;

	public SpendingAccount(int id, int sum, String currency) 
	{
		super(id, sum, currency);
	}
	
	public int withdraw(int sum) 
	{
		if(this.getSum() < sum || sum < 0) 
		{
			return -1;
		}
		else
		{
			this.setSum(this.getSum() - sum);
			return 1;
		}
	}
	
	public boolean deposit(int sum) 
	{
		if(this.getSum() < 0) 
		{
			return false;
		}
		else 
		{
			this.setSum(this.getSum() + sum);
			return true;
		}
	}
	
	public Object[] toRowForm() 
	{
		Object[] row =  {this.getId(),"","","Spending",this.getSum(), "-" , this.getCurrency()};
		return row;
	}
}
