package TableModels;

import javax.swing.table.AbstractTableModel;
public class PersonModel extends AbstractTableModel
{
	private static final long serialVersionUID = 584852758754803197L;
	private Object[] columnNames;
	private Object[][] data;
	private String objectName;
	
	public PersonModel( Object[][] data) 
	{
		this.columnNames = new Object[2];
		columnNames[0] = "ID";
		columnNames[1] = "Name";
		this.data = data;
	}
	
	public void setData(Object[][] data)
	{
		this.data = data;
		this.fireTableDataChanged();
	}
	
	@Override
	public int getColumnCount()
	{
        return columnNames.length;
    }
	
	@Override
    public boolean isCellEditable(int row, int column)
	{   // custom isCellEditable function
        return column == 1;
    }
	
    public int getRowCount() 
    {
        return data.length;
    }

    public String getColumnName(int col) 
    {
        return columnNames[col].toString();
    }

    public Object getValueAt(int row, int col)
    {
        return data[row][col];
    }
    
    public void setValueAt(Object value, int row, int col)
    {
        data[row][col] = value;
        fireTableCellUpdated(row, col);
    }
    
    public String getObjectName() 
    {
    	return objectName;
    }
   
    

}
