package Domain;

import java.io.Serializable;

public class Person implements Serializable, Comparable<Person> 
{

	private static final long serialVersionUID = 5991108375546466101L;
	private int id;
	private String name;
	
	public Person(int id, String name) 
	{
		this.setId(id);
		this.setName(name);
	}

	public int getId() 
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}
	
	//in combobox
	public String toString() 
	{
		return id + "-" + name;
	}
	
	//pt tabel - un rand
	public Object[] toRowForm() 
	{
		Object[] row = {id,name};
		return row;
	}
	
	@Override
	public int compareTo(Person o) 
	{
		return ( this.getId() - o.getId());
	}
	
}
