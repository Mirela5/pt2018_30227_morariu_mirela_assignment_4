package Domain;

import java.io.Serializable;

public class Account implements Serializable, Comparable<Account>
{
	private static final long serialVersionUID = 4813596837534136228L;
	private int id;
	private int sum;
	private String currency;
	
	public Account(int id, int sum, String currency)
	{
		this.setId(id);
		this.setSum(sum);
		this.setCurrency(currency);
	}
	
	public int getId() 
	{
		return id;
	}
	
	void setId(int id) 
	{
		this.id = id;
	}
	
	public int getSum() 
	{
		return sum;
	}
	
	void setSum(int sum) 
	{
		this.sum = sum;
	}
	
	String getCurrency() 
	{
		return currency;
	}
	
	void setCurrency(String currency)
	{
		this.currency = currency;
	}
	
	// default
	public int withdraw(int sum) 
	{
		return 0;
	}
	
	// default
	public boolean deposit(int sum) 
	{
		return false;
	}
	
	//default
	public Object[] toRowForm() 
	{
		return null;
	}

	@Override
	public int compareTo(Account a) 
	{
		return this.getId() - a.getId();
	}
	

}