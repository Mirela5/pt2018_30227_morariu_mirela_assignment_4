package UI;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import Bank.Bank;
import Domain.Account;
import Domain.Person;
import Domain.SavingAccount;
import Domain.SpendingAccount;
import TableModels.AccountModel;
import UI.MainGUI;

public class AccountGUI extends JFrame
{
	private static final long serialVersionUID = 7182819592224280053L;
	JButton exit;
	JButton list,add;
	JLabel label, label0, label1, label2;
	JTextField textField1, textField2, textField4, textField5;
	JTextArea textArea;
	JTable table;
	JComboBox<Object> user, saving, currency;
	Bank bank;
	
	public AccountGUI(Bank b) 
	{
			bank = b;
		    this.setSize(720, 480);
			this.setLocationRelativeTo(null);
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setTitle("Accounts"); 
			this.setVisible(true);
			this.setLayout(null);
			
			Color customColor1 = new Color(229,38,23);
			Color customColor = new Color(166,189,219);
			Color customColor2 = new Color(51, 166, 94);
			
			add = new JButton("ADD");
	        add.setBounds(570, 300, 100, 40);
		    add.setBackground(customColor2);
			ListenForButton foradd = new ListenForButton();
			add.addActionListener(foradd);
			this.add(add);
			
			exit = new JButton("BACK");
			exit.setForeground(new Color(255,255,255));
	        exit.setBounds(570, 360, 100, 40);
		    exit.setBackground(customColor1);
			ListenForButton forexit = new ListenForButton();
			exit.addActionListener(forexit);
			this.add(exit);

			String op[] = this.getPersons();
			user = new JComboBox<Object>(op);
			JPanel p = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
			p.setBounds(50, 250, 90 , 25);			
			p.add(user);
			this.add(p);
			
			String op1[] = {"Savings","Spending"};
			saving = new JComboBox<Object>(op1);
			JPanel p1 = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
			p1.setBounds(135, 250, 90 , 25);			
			p1.add(saving);
			this.add(p1);
			
			textField4 = new JTextField("",9);
			textField4.requestFocus();
			textField4.setText("Amount");
			this.add(textField4);
			textField4.setBounds(230, 250, 100 , 26);
			textField4.setForeground(Color.GRAY);
			textField4.addFocusListener(new FocusListener() 
			{
			    @Override
			    public void focusGained(FocusEvent e) 
			    {
			        if (textField4.getText().equals("Amount")) 
			        {
			        	textField4.setText("");
			        	textField4.setForeground(Color.BLACK);
			        }
			    }
			    
			    @Override
			    public void focusLost(FocusEvent e)
			    {
			        if (textField4.getText().isEmpty()) 
			        {
			        	textField4.setForeground(Color.GRAY);
			        	textField4.setText("Amount");
			        }
			    }
		    });
			
			String op2[] = {"RON", "EUR", "CHF"};
			currency = new JComboBox<Object>(op2);
			JPanel p2 = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
			p2.setBounds(315, 250, 90 , 25);			
			p2.add(currency);
			this.add(p2);
			this.setVisible(true);
			
			table = new JTable(new AccountModel(this.getData()));
		    table.setLayout(null);
		    table.getModel().addTableModelListener(new Listener());
		    JScrollPane scroll = new JScrollPane(table); 
		    scroll.setBounds( 50, 20,600,210);
			this.add(scroll);
			this.setVisible(true);
			
			textArea = new JTextArea();
			textArea.setBackground(customColor);
			JScrollPane scrollPane = new JScrollPane(textArea);
		    scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			scrollPane.setBackground(customColor);
			this.add(scrollPane);
			scrollPane.setBounds(50,300,400,100);

	}
	
	public Object[][] getData() // matrice cu toate conturile
	{
		Object[] temp;
		ArrayList<Object[]> array = new ArrayList<>();
		for(Person i : bank.getPersons())
		{
			for(Account j: bank.getAccounts(i))
	 		{
				temp = j.toRowForm();
				temp[1] = i.getId();
				temp[2] = i.getName();
				array.add(temp);
			}
		}
		
		Object[][] toReturn = new Object[array.size()][6]; 
		for(int i = 0 ; i < array.size(); i++)
		{
			toReturn[i] = array.get(i);
		}
		return toReturn;
	}
	
	public void display()
	{
		dispose();
		this.setSize(720, 480);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("List customer"); 
		this.setVisible(true);
		this.setLayout(null);
		
	}
	
	public String[] getPersons() // lista de stringuri cu persoane pt combobox
	{
		ArrayList<Person> array  = new ArrayList<>(bank.getPersons());
		Collections.sort(array);
		String[] toReturn = new String[array.size()];
		for(int i = 0 ; i < array.size(); i++) 
		{
			toReturn[i] = array.get(i).toString();
		}
		return toReturn;
	}
	
	public static void main(String[] args) 
	{
		 new AccountGUI(new Bank());
	}
	
	private class Listener implements TableModelListener 
	{
		@Override
		public void tableChanged(TableModelEvent e) 
		{
			if(e.getType() !=TableModelEvent.DELETE	)
			{
				int row = e.getFirstRow();
				AccountModel m = (AccountModel) e.getSource();
				
			    boolean delete = true;
			    if(m.getValueAt(row, 4).toString().trim().length() != 0) // verifica campul ammount
			    	delete = false;
			    if(m.getValueAt(row, 6).toString().trim().length() != 0) // verifica campul currency
			    	delete = false;
			    
			    if(!delete) 
			    {
			    	Account a2;
			    	int accountID = Integer.parseInt(m.getValueAt(row, 0).toString());
			    	int personID = Integer.parseInt(m.getValueAt(row, 1).toString());
			    	int ammount = Integer.parseInt(m.getValueAt(row, 4).toString());
			    	String Curency = m.getValueAt(row, 6).toString();
			    	boolean savings = m.getValueAt(row, 3).toString() == "Savings";
			    	System.out.println(savings + " " + m.getValueAt(row, 3));
			    	if(savings)		    	
			    		a2 = new SavingAccount(accountID, ammount, Curency);
			    	else		    	
			    		a2 = new SpendingAccount(accountID, ammount, Curency);
			    	
			    	bank.updateAccount(bank.getAccountById(accountID), a2 , bank.getPersonById(personID));
		     }
		     else 
		     {
		    	int accountID = Integer.parseInt(m.getValueAt(row, 0).toString());
		    	int personID = Integer.parseInt(m.getValueAt(row, 1).toString());
		    	bank.removeAccount(bank.getAccountById(accountID),bank.getPersonById(personID));
		    	m.setData(getData());
		     }
		   }
		}
    }
	
	private class ListenForButton implements ActionListener
	{			
		public void actionPerformed(ActionEvent e) 
		{
			if (e.getSource() == exit)
			{
					dispose();
					try 
					{
						new MainGUI(bank).setVisible(true);
					} 
					catch (IOException e1) 
					{
						e1.printStackTrace();
					}
		    }
			else if(e.getSource() == add) 
			{
				int userID = Integer.parseInt(user.getSelectedItem().toString().split("-")[0]);
				boolean accountType = (saving.getSelectedItem().toString() == "Savings");
				String _currency = currency.getSelectedItem().toString();
				int ammount = -1;
				int id = bank.getNextAccountId();
				
				try 
				{
					ammount = Integer.parseInt(textField4.getText());
				}
				catch(Exception exc) 
				{
					textArea.append("ERROR: Amount should be a number");
				}
				
				if(ammount != -1) 
				{
					if(accountType == false) 
					{
						// spending
						bank.addAccount(new SpendingAccount(id, ammount, _currency), bank.getPersonById(userID));
					}
					else
					{
						// savings
						bank.addAccount(new SavingAccount(id, ammount, _currency), bank.getPersonById(userID));
					}
					
					AccountModel m = (AccountModel)table.getModel();
					m.setData(getData());
				}
			}
		}
	}
}
