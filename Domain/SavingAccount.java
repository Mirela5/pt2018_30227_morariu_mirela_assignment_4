package Domain;

// SavingAccount only has one deposit and one withdraw
public class SavingAccount extends Account 
{
	private static final long serialVersionUID = 2921622505440190323L;
	double interestRate; // dobanda
	
	public SavingAccount(int id, int sum, String currency) 
	{
		super(id, sum , currency);
		this.interestRate = this.calcInterest();
		this.setSum((int)(Math.floor(sum*interestRate)));
	}
	
	private double calcInterest() 
	{
		double interestRate = 1;
		double coeficient = 0.00002;
		if(this.getSum()  < 1000 )
		{
			coeficient *= 2;
		}
		else if(this.getSum() > 1000 && this.getSum() < 50000) 
		{
			coeficient /= 2;
		}
		else
		{
			coeficient /= 10000;
		}
		// calc interest rate;
		interestRate += getSum() * coeficient;
		return interestRate;
	}
	
	public int withdraw(int sum) 
	{
		sum = this.getSum();
		this.setSum(0);
		return sum;
	}
	
	public boolean deposit(int sum) 
	{
		return false;
	}
	
	public Object[] toRowForm() 
	{
		Object[] row =  {this.getId(),"","","Savings",this.getSum(), this.interestRate , this.getCurrency()};
		return row;
	}	
}
