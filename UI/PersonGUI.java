package UI;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import Bank.Bank;
import Domain.Person;
import TableModels.PersonModel;
import UI.MainGUI;

public class PersonGUI extends JFrame
{
	private static final long serialVersionUID = 7182819592224280053L;
	JButton exit;
	JButton list,add;
	JLabel label, label0, label1, label2;
	JTextField textField1, textField2, textField3;
	JTextArea textArea;
	JTable table;
	Bank bank;
	
	public PersonGUI(Bank b) 
	{
			this.bank = b;
		    this.setSize(720, 480);
			this.setLocationRelativeTo(null);
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setTitle("Persons"); 
			this.setVisible(true);
			this.setLayout(null);
			
			Color customColor1 = new Color(229,38,23);
			Color customColor = new Color(166,189,219);
			Color customColor2 = new Color(51, 166, 94);
			
			add = new JButton("ADD");
	        add.setBounds(570, 300, 100, 40);
		    add.setBackground(customColor2);
			ListenForButton foradd = new ListenForButton();
			add.addActionListener(foradd);
			this.add(add);
			
			exit = new JButton("BACK");
			exit.setForeground(new Color(255,255,255));
	        exit.setBounds(570, 360, 100, 40);
		    exit.setBackground(customColor1);
			ListenForButton forexit = new ListenForButton();
			exit.addActionListener(forexit);
			this.add(exit);
			
		    table = new JTable(new PersonModel(this.getData()));
		    table.setLayout(null);
		    table.getModel().addTableModelListener(new Listener());
			
		    JScrollPane scroll = new JScrollPane(table); 
		    scroll.setBounds( 50, 20,600,210);
			this.add(scroll);
			this.setVisible(true);
			
			textField2 = new JTextField("", 9);
			textField2.requestFocus();
			this.add(textField2);
			textField2.setBounds(340,250, 297, 20);
	
			textArea = new JTextArea();
			textArea.setBackground(customColor);
			JScrollPane scrollPane = new JScrollPane(textArea);
		    scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			scrollPane.setBackground(customColor);
			this.add(scrollPane);
			scrollPane.setBounds(90,300,400,100);
	}
	
	// aduce datele din hashmap
	private Object[][] getData() 
	{
		// lista de persoane din banca
		ArrayList<Person> array  = new ArrayList<>(bank.getPersons());
		Collections.sort(array);
		Object[][] toReturn = new Object[array.size()][2]; // matrice de afisat
		int pos = 0;
		for(Person i : array) 	// parcurgere pentru adaugare date in matrice
		{	
			toReturn[pos] = i.toRowForm();  
			pos++;
		}
		return toReturn;
	}
	
	public void display()
	{
		dispose();
		this.setSize(720, 480);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("List customer"); 
		this.setVisible(true);
		this.setLayout(null);
	}
	
	public static void main(String[] args) 
	{
		 new PersonGUI(new Bank());
	}
	
	private class ListenForButton implements ActionListener
	{				
		public void actionPerformed(ActionEvent e) 
		{

		
			if (e.getSource() == exit)
			{
				dispose();
				try 
				{
					new MainGUI(bank).setVisible(true);
				}
				catch (IOException e1)
				{
					e1.printStackTrace();
				}
		    }
			else if(e.getSource() == add) 
			{
				if(textField2.getText().trim().length() == 0)	// verf daca numele nu e gol
				{ 
					textArea.setText("Enter a name");  
				}
				else 
				{
					bank.addPerson(new Person(bank.getNextPersonId(),textField2.getText())); 
					PersonModel m = (PersonModel)table.getModel();
					m.setData(getData()); // refresh table
				}
			}
		}
	}
	
	private class Listener implements TableModelListener 
	{
		@Override
		// pentru schimbarea datelor din tabel
		public void tableChanged(TableModelEvent e) 
		{
			if(e.getType() !=TableModelEvent.DELETE	)
			{
				 int row = e.getFirstRow();
			     PersonModel m = (PersonModel) e.getSource();   // tabel pentru afisarea persoanelor
			     if(m.getValueAt(row, 1).toString().trim().length() != 0) // verif daca e goala celula
			     {
			    	 Person p1 = bank.getPersonById(Integer.parseInt(m.getValueAt(row, 0).toString())); 
			    	 // update in banca pentru persoana
			    	 bank.updatePerson(p1,new Person(p1.getId(),m.getValueAt(row, 1).toString())); 
			     }
			     else 
			     {
			    	 bank.removePerson(bank.getPersonById(Integer.parseInt(m.getValueAt(row, 0).toString())));
			    	 m.setData(getData()); // refresh
			     }
			}
		}
    }
}
