package Bank;

import java.util.ArrayList;
import java.util.Set;

import Domain.Account;
import Domain.Person;

public interface BankProc 
{
	void addPerson(Person p);
	void updatePerson(Person p1, Person p2);
	void removePerson(Person p);
	
	void addAccount(Account a, Person p);
	void updateAccount(Account a1, Account a2, Person p);
	void removeAccount(Account a, Person p);
	
	ArrayList<Account> getAccounts (Person p);

	Set<Person> getPersons();

	int getNextPersonId();
	int getNextAccountId();
	
	Account getAccountById(int id);
	Person getPersonById(int id);
}
