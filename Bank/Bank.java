package Bank;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import Domain.Account;
import Domain.Person;

public class Bank implements BankProc 
{
	HashMap<Person,ArrayList<Account>> data;

	public Bank() 
	{
		data = new HashMap<>();
		loadData();
	}
	
	private void saveData() 
	{
		try 
		{
			FileOutputStream file = new FileOutputStream("src/data.ser"); // file descriptor
			ObjectOutputStream stream = new ObjectOutputStream(file);// conexiunea intre fisier si app
			stream.writeObject(data);// serializeaza hashmap -> serializeaza toate obiectele din hashmap
			stream.close();
			file.close();
			
		} 
		catch (IOException e) 
		{
			System.out.print("File not found!");
		}
	}
	
	@SuppressWarnings("unchecked")
	private void loadData() 
	{
		try 
		{
			FileInputStream file = new FileInputStream("src/data.ser");
			ObjectInputStream stream = new ObjectInputStream(file);
			data = (HashMap<Person, ArrayList<Account>>) stream.readObject();
			stream.close();
			file.close();
		} 
		catch (IOException | ClassNotFoundException e)
		{
			System.out.print("File not found!");
		}
	}
	
	@Override
	public void addPerson(Person p)
	{
		assert(p != null && !data.containsKey(p));
		data.put(p, new ArrayList<Account>());
		saveData();
		assert(data.containsKey(p));
	}
	
	@Override
	public void updatePerson(Person p1, Person p2)
	{
		// preconditie
		assert(p1 != null && p2 != null && data.containsKey(p1) && !data.containsKey(p2));
		// adauga in hashmap conturile lui p1 la cheia p2;
		data.put(p2, data.get(p1));
		// p1
		data.remove(p1);
		saveData();
		// postconditie
		assert(!data.containsKey(p1) && data.containsKey(p2));		
	}
	
	@Override
	public void removePerson(Person p)
	{
		assert(p != null && data.containsKey(p));
		data.remove(p);
		saveData();
		assert(!data.containsKey(p));
	}

	@Override
	public void addAccount(Account a, Person p)
	{
		assert(p != null && a != null && data.containsKey(p) && !data.get(p).contains(a));
		data.get(p).add(a);
		saveData();
		assert(data.get(p).contains(a));
	}

	@Override
	public void updateAccount(Account a1, Account a2, Person p) 
	{
		assert(p != null && a1 != null && a2 != null 
				&& data.containsKey(p) && !data.get(p).contains(a2) && data.get(p).contains(a1));
		// get list of accounts for person p;
		ArrayList<Account> toUpdate = data.get(p);
		//put a2 in the position a1 was before;
		toUpdate.set(toUpdate.indexOf(a1), a2);
		saveData();
		assert(data.get(p).contains(a2) && !data.get(p).contains(a1));
	}

	@Override
	public void removeAccount(Account a, Person p)
	{
		assert(p != null && a != null && data.containsKey(p) && data.get(p).contains(a));
		data.get(p).remove(a);
		saveData();
		assert(!data.get(p).contains(a));
	}

	@Override
	public ArrayList<Account> getAccounts(Person p) 
	{
		assert(p != null && data.containsKey(p));
		return data.get(p);
	}

	@Override
	public Set<Person> getPersons() 
	{
		// returneaza o multime de key - persoane
		return data.keySet();
	}
	
	@Override
	public int getNextPersonId() 
	{
		int nextId = -1 ;
		for(Person i : this.getPersons())
			if(i.getId() > nextId)
				nextId = i.getId();
		return nextId + 1;
	}

	@Override
	public int getNextAccountId() 
	{
		int nextId = -1 ;
		for(Person i : this.getPersons())
			for(Account j : this.getAccounts(i))
				if(j.getId() > nextId)
					nextId = j.getId();
		return nextId + 1;
	}

	@Override
	public Account getAccountById(int id)
	{
		for(Person i : this.getPersons())
			for(Account j : this.getAccounts(i))
				if(j.getId() == id)
				return j;	
		return null;
	}

	@Override
	public Person getPersonById(int id)
	{
		for(Person i : this.getPersons())
			if(i.getId() == id)
				return i;
		return null;
	}
}
