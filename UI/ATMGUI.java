package UI;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import Bank.Bank;
import Domain.Account;
import Domain.Person;
import Domain.SavingAccount;
import Domain.SpendingAccount;
import UI.MainGUI;

public class ATMGUI extends JFrame
{	
	private static final long serialVersionUID = 7182819592224280053L;
	JButton exit;
	JButton withdraw;
	JButton deposit;
	JLabel label0,label1;
	JTextArea textArea;
	JTextField amount;
	Bank bank;
	JComboBox<Object> persons, accounts;
	
	public ATMGUI(Bank b) 
	{
			this.bank = b;
		    this.setSize(720, 480);
			this.setLocationRelativeTo(null);
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setTitle("ATM"); 
			this.setLayout(null);
			
			Color customColor1 = new Color(229,38,23);
			Color customColor = new Color(166,189,219);
			Color customColor2 = new Color(51, 166, 94);
			
			amount = new JTextField("",9);
			amount.requestFocus();
			amount.setText("Amount");
			this.add(amount);
			amount.setBounds(290, 200, 100 , 26);
			amount.setForeground(Color.GRAY);
			amount.addFocusListener(new FocusListener() 
			{
			    @Override
			    public void focusGained(FocusEvent e) 
			    {
			        if (amount.getText().equals("Amount")) 
			        {
			        	amount.setText("");
			        	amount.setForeground(Color.BLACK);
			        }
			    }
			    
			    @Override
			    public void focusLost(FocusEvent e) 
			    {
			        if (amount.getText().isEmpty()) 
			        {
			        	amount.setForeground(Color.GRAY);
			        	amount.setText("Amount");
			        }
			    }
		    });
			
			withdraw = new JButton("WITHDRAW");
			withdraw.setBounds(570, 200, 100, 40);
			withdraw.setBackground(customColor2);
			withdraw.setForeground(Color.white);
			ListenForButton foradd = new ListenForButton();
			withdraw.addActionListener(foradd);
			this.add(withdraw);
			
			deposit = new JButton("Deposit");
			deposit.setBounds(570, 250, 100, 40);
			deposit.setBackground(customColor2);
			deposit.setForeground(Color.white);
			ListenForButton forDeposit = new ListenForButton();
			deposit.addActionListener(forDeposit);
			this.add(deposit);
			
			exit = new JButton("BACK");
			exit.setForeground(new Color(255,255,255));
	        exit.setBounds(570, 360, 100, 40);
		    exit.setBackground(customColor1);
			ListenForButton forexit = new ListenForButton();
			exit.addActionListener(forexit);
			this.add(exit);
			
			String op[] = this.getPersons();
			persons = new JComboBox<Object>(op);
			persons.addItemListener(new ItemListener()
			{
				@Override
				public void itemStateChanged(ItemEvent e)
				{
					if (e.getStateChange() == ItemEvent.SELECTED) 
					{
						// aduce toate conturile persoanei cu id selectat in combobox
						String[] op1 = getAccounts(Integer.parseInt(e.getItem().toString().split("-")[0]));
				    	accounts.removeAllItems();	// stergem toate item-urile 
				    	for(int i = 0 ; i < op1.length ;i++)
				    		accounts.addItem(op1[i]);
					}
				}
			});
			
			JPanel p = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
			p.setBounds(90, 200, 90 , 25);			
			p.add(persons);
			this.add(p);
			String[] op1;
			
			try
			{
				op1 = this.getAccounts(Integer.parseInt(op[0].split("-")[0]));
			}
			catch(Exception e)
			{
				op1 = new String[1];
			}
			
			accounts = new JComboBox<Object>(op1);
			accounts.addItemListener(new ItemListener() 
			{
				@Override
				public void itemStateChanged(ItemEvent e)
				{
					// afiseaza cand se selecteaza un cont din combo 
					if (e.getStateChange() == ItemEvent.SELECTED) 
					{
						textArea.append("------------------------------------------ $$$$$ ------------------------------------------\n");
						Account a = bank.getAccountById(Integer.parseInt(e.getItem().toString().trim()));
						String accountType = "Savings";
						if(a.getClass() == SpendingAccount.class) 
						{
							accountType = "Spending";
						}
						textArea.append("Account type: " + accountType + "\n");
						textArea.append("Amount in account before: " + a.getSum() + "\n");
					}
				}	
			});
		
			JPanel p1 = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
			p1.setBounds(180, 200, 90 , 25);			
			p1.add(accounts);
			this.add(p1);
			
			textArea = new JTextArea();
			textArea.setBackground(customColor);
			JScrollPane scrollPane = new JScrollPane(textArea);
		    scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			scrollPane.setBackground(customColor);
			this.add(scrollPane);
			scrollPane.setBounds(90,300,400,100);
			this.setVisible(true);
	}
	
	public void display()
	{
		dispose();
		this.setSize(720, 480);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("ATM"); 
		this.setVisible(true);
		this.setLayout(null);
	}
	
	public String[] getPersons() 
	{
		ArrayList<Person> array  = new ArrayList<>(bank.getPersons());
		Collections.sort(array);
		String[] toReturn = new String[array.size()];
		for(int i = 0 ; i < array.size(); i++)
		{
			toReturn[i] = array.get(i).toString();
		}
		return toReturn;
	}
	
	public String[] getAccounts(int personID)
	{
		Person p = bank.getPersonById(personID);
		ArrayList<Account> array  = bank.getAccounts(p);
		Collections.sort(array);
		String[] toReturn = new String[array.size()];
		for(int i = 0 ; i < array.size(); i++) 
		{
			toReturn[i] = Integer.toString(array.get(i).getId());
		}
		return toReturn;
	}
	
	public static void main(String[] args) 
	{
		 new ATMGUI(new Bank());
	}
	
	private class ListenForButton implements ActionListener
	{				
		public void actionPerformed(ActionEvent e) 
		{

			if (e.getSource() == exit)
			{
				dispose();
				try 
				{
					new MainGUI(bank).setVisible(true);
				} 
				catch (IOException e1) 
				{
					e1.printStackTrace();
				}
		    }
			else if(e.getSource() == withdraw) 
			{
				Account a = bank.getAccountById(Integer.parseInt(accounts.getSelectedItem().toString()));
				
				try 
				{
					int sum = Integer.parseInt(amount.getText());
					if(sum > a.getSum() && a.getClass() == SpendingAccount.class)
					{
						textArea.append("Insuficient Funds!!! \n");
					}
					else 
					{
						a.withdraw(sum);
						textArea.append("Amount in account after: " + a.getSum() + "\n");
						textArea.append("------------------------------------------ $$$$$ ------------------------------------------\n");
					}
				}catch(Exception E)
				{
					textArea.append("Please enter a number\n");
				}
			}
			else if(e.getSource() == deposit)
			{
				Account a = bank.getAccountById(Integer.parseInt(accounts.getSelectedItem().toString()));
				try 
				{
					int sum = Integer.parseInt(amount.getText());
					if(a.getClass() == SavingAccount.class) 
					{
						textArea.append("Only one deposit can be done\n");
					}
					else 
					{
						a.deposit(sum);
						textArea.append("Amount in account after: " + a.getSum() + "\n");
						textArea.append("------------------------------------------ $$$$$ ------------------------------------------\n");
					}
				}
				catch(Exception E)
				{
						textArea.append("Please enter a number\n");
				}
	       }	
	   }
    }
}
